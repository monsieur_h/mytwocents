            /**
             * Global vars
             */
            var debugConsole    =   true;//TODO: pass false when in prod
            var CURRENCY        =   "&euro;";
            var LOCAL_STORAGE   =   false;
            var TICK            =   false;
            var DISPLAY_BUTTON  =   false;
            var NOTIF           =   false;
            var NOTIF_CAPABLE   =   false;
            var TIMERLAP        =   10;
            var counterNames    =   Array();
            var counterDates    =   Array();
            var counterIncomes  =   Array();
            
            /**
            * Fonction de debug developpeur, console si dispo, et sinon en window.alert(). Ne marche que si debug=true;
            * @param str string a afficher pour le debug
            */
            function debug(str){
               if (debugConsole===true) {
                   if (console.log) {
                       console.log(str);
                   }else{
                       window.alert(str);
                   }
               }
           }
           
            /**
             * Adds a counter to the list based on give name and income. Starts on current time
             */
            function addCounter(){
                var name = $("#counterName").val();
                var income = $("#monthIncome").val();
                var startDate = new Date().getTime();
                if (name.length === 0 || isNaN(income)) {
                    $("#errorMessage").html("Looks like you forgot a field buddy...");
                    $.mobile.changePage("#errorDialog",{transition: 'pop', role: 'dialog'});
                }else{
                    $("#counterName").val("");
                    $("#monthIncome").val("");

                    counterNames.push(name);
                    counterDates.push(startDate);
                    counterIncomes.push(income);
                    saveCounterValue();
                    $.mobile.changePage("#main");
                }
            }
            
            /**
             * Removes counter with its identifier
             */
            function removeCounter(identifier){
                debug("Removing counter...");
                index = identifier.split("-")[0];
                if (index !== -1) {
                    counterDates.splice(index,1);
                    counterIncomes.splice(index,1);
                    counterNames.splice(index,1);
                    saveCounterValue();
                }
            }
            
            function resetCounter(identifier){
                debug("Reseting counter");
                index = identifier.split("-")[0];
                if (index !== -1) {
                    counterDates[index] = new Date().getTime();
                    saveCounterValue();
                }
            }
            
            /**
             * Saves counters info into whatevers it's stored
             */
            function saveCounterValue(){
                debug("Saving counters...");
                if (LOCAL_STORAGE) {
                    localStorage.setItem("counterNames"     ,counterNames.join(","));
                    localStorage.setItem("counterDates"     ,counterDates.join(","));
                    localStorage.setItem("counterIncomes"   ,counterIncomes.join(","));
                }else{
                    $.cookie("counterNames"     ,counterNames.join(","),    { expires: 300 });
                    $.cookie("counterDates"     ,counterDates.join(","),    { expires: 300 });
                    $.cookie("counterIncomes"   ,counterIncomes.join(","),  { expires: 300 });
                }
                
            }

            /**
             * Reads counter value from wherever it's stocked
             */
            function readCounterValue(){
//                debug("Reading counters...");
                if (LOCAL_STORAGE) {
                    counterNames    =   localStorage.getItem("counterNames")    ? localStorage.getItem("counterNames").split(",") : new Array();
                    counterDates    =   localStorage.getItem("counterDates")    ? localStorage.getItem("counterDates").split(",") : new Array();
                    counterIncomes  =   localStorage.getItem("counterIncomes")  ? localStorage.getItem("counterIncomes").split(",") : new Array();
                }else{
                    counterNames    =   $.cookie("counterNames")    ? $.cookie("counterNames").split(",") : new Array();
                    counterDates    =   $.cookie("counterDates")    ? $.cookie("counterDates").split(",") : new Array();
                    counterIncomes  =   $.cookie("counterIncomes")  ? $.cookie("counterIncomes").split(",") : new Array();
                }
                
            }
            
            /**
             * Saves parameter to whatever
             */
            function saveParamValue(){
               if (LOCAL_STORAGE) {
                   localStorage.setItem("LOCAL_STORAGE", LOCAL_STORAGE);
                   localStorage.setItem("CURRENCY", CURRENCY);
                   localStorage.setItem("TICK", TICK);
                   localStorage.setItem("NOTIF", NOTIF);
                   localStorage.setItem("DISPLAY_BUTTON", DISPLAY_BUTTON);
                   debug(TIMERLAP);
                   localStorage.setItem("TIMERLAP", TIMERLAP);
                }else{
                    $.cookie("LOCAL_STORAGE", LOCAL_STORAGE);
                    $.cookie("CURRENCY", CURRENCY);
                    $.cookie("TICK", TICK);
                    $.cookie("NOTIF", NOTIF);
                    $.cookie("DISPLAY_BUTTON", DISPLAY_BUTTON);
                    $.cookie("TIMERLAP", TIMERLAP);
                }
                applyParams();
            }
            
            /**
             * Applies read parameters to the application
             */
            function applyParams(){
                debug("Applying params");

                debug("DISPLAY_BUTTON\t"+   DISPLAY_BUTTON);
                debug("CURRENCY\t"+         CURRENCY);
                debug("LOCAL_STORAGE\t" +   LOCAL_STORAGE);
                debug("TICK\t\t"        +   TICK);
                debug("NOTIF_CAPABLE\t" +   NOTIF_CAPABLE);
                debug("LOCAL_STORAGE\t" +   LOCAL_STORAGE);
                debug("TIMERLAP\t"      +   TIMERLAP);
                if(DISPLAY_BUTTON){
                    $("#lastListElement").show();
                    $("#show-button").attr("checked","checked");
                }else{
                    $("#lastListElement").hide();
                    $("#hide-button").attr("checked","checked");
                }
                
                if (TICK) {
                    $("#tickerParam").show();
                    $("#tick-button").attr("checked","checked");
                }else{
                    $("#tickerParam").hide();
                    $("#notick-button").attr("checked","checked");
                }
                
                if (NOTIF) {
                    $("#notif-button").attr("checked","checked");
                }else{
                    $("#nonotif-button").attr("checked","checked");
                }
//                debug("tl is"+TIMERLAP);
//                debug(typeof(TIMERLAP));
                $("#timerLap").val(""+TIMERLAP);
//                $("#timerLap").slider('refresh');
            }
            
            /**
             * Reads the chose currency symbol value from wherever it's stocked. Default is euro
             */
            function readParamsValue(){
                debug("Reading params...");
                //Check if the string stored is "true", if not, that's false
                //Used because we can only store string in storage/cookies
                if (localStorage) {
                    CURRENCY        =   localStorage.getItem("CURRENCY")==="true"       ?true:CURRENCY;
                    LOCAL_STORAGE   =   true;
                    TICK            =   localStorage.getItem("TICK")==="true"           ?true:false;
                    DISPLAY_BUTTON  =   localStorage.getItem("DISPLAY_BUTTON")==="true" ?true:false;
                    NOTIF           =   localStorage.getItem("NOTIF")==="true"          ?true:false;
                    debug(TIMERLAP);
                    TIMERLAP        =   isNaN(parseInt(localStorage.getItem("TIMERLAP")))?TIMERLAP:parseInt(localStorage.getItem("TIMERLAP"));//TODO rewrite
                    $("#localStorageStatus").html("Great! Your browser can use localStorage. You maybe won't see the difference, but I will!");
                    debug(TIMERLAP);
                } else {
                    CURRENCY        =   $.cookie("CURRENCY")==="true"       ?true:CURRENCY;
                    LOCAL_STORAGE   =   false;
                    TICK            =   $.cookie("TICK")==="true"           ?true:false;
                    DISPLAY_BUTTON  =   $.cookie("DISPLAY_BUTTON")==="true" ?true:false;
                    NOTIF           =   $.cookie("NOTIF")==="true"          ?true:false;
                    TIMERLAP        =   isNaN(parseInt($.cookie("TIMERLAP")))?TIMERLAP:parseInt($.cookie("TIMERLAP"));//TODO rewrite
                    $("#localStorageStatus").html("Your browser doens't have a localStorage. Data will be stored in cookies. If they get erased, you'll loose your data");
                }
                applyParams();
            }
            
            /**
             * Converts a monthly income to a millisecond income
             * @param  income monthly income
             * @return income per millisecond
             */
            function monthlyToMillisecIncome(income){
                var DAYS_PER_YEAR       =   365.25;
                var MONTH_PER_YEAR      =   12;
                var HOURS_PER_DAY       =   24;
                var MINUTES_PER_HOUR    =   60;
                var SEC_PER_MINUTE      =   60;
                var MILLISEC_PER_MINUTE =   1000;
                return income * MONTH_PER_YEAR / DAYS_PER_YEAR / HOURS_PER_DAY / MINUTES_PER_HOUR / SEC_PER_MINUTE / MILLISEC_PER_MINUTE;
            }
            
            var last_tick;
            /**
             * Handles Ticking
             * @param money money amount to be notified 
             */
            function handleTicking(money){
                debug("TICKING!!");
                if (last_tick == Math.round(money)) {//TODO: you can do better than this
                    debug("Tick sur la même valeur: on ne fait rien");
                    return;
                }
                last_tick = Math.round(money);  
                if(TICK){
                    //TODO:animate
                    //TODO:sound?
                    if(NOTIF){
                        //NOTIF_CAPABLETODO:HTML5NOTIF
                        //navigator.notification.alert("You just earned"+money+CURRENCY);
                        debug("NOTIFYING:");
                        debug("You just earn "+money+" more "+CURRENCY);
                        var noti = window.webkitNotifications.createNotification(
                                            "../style/images/coins.png",
                                            "Congrats!",
                                            "You just earn "+TIMERLAP+" more coin (total: "+money+")");
                        noti.show();
                        setTimeout(function(){
                            noti.cancel();
                        }, '3000');
                    }
                }
            }

            /**
             * Refreshes the list of counters. Either updating them or creating elements
             */
            var detailId;
            function refreshCounterList(){
//                debug("Refreshing list...");
                readCounterValue();
                
                for(i = 0 ; i < counterNames.length ; i++){
                    thatStart   =    counterDates[i];
                    thatIncome  =    counterIncomes[i];
                    thatName    =    counterNames[i];
                    identifier  =    i+"-counter";
                    
                    if ($("#"+identifier).length !== 0) {
//                        debug("Refreshing UI element...");
//                        debug("detailed id ="+detailId);
                        delta = new Date().getTime() - counterDates[i]; 
                        
                        millisecIncome = monthlyToMillisecIncome(thatIncome);
                        sofar = delta * millisecIncome;
                        sofar = Math.round(sofar * 100) / 100;
                        
                        tmpDate = new Date(delta);
                        elapsed = tmpDate.getUTCHours() +":"+tmpDate.getUTCMinutes()+ ":"+tmpDate.getUTCSeconds();

//                        debug("SO FAR:\t" +      delta * millisecIncome);
                        
                        if(identifier === detailId){
                            $("#detail-elapsed").html(elapsed);
                            $("#detail-earned").html(sofar + CURRENCY);
                            $("#detail-name").html(thatName);
                            startDate = new Date();
                            startDate.setTime(thatStart);
                            dateString = startDate.getDay()+"/"+startDate.getMonth()+"/"+startDate.getYear();
                            $("#detail-date").html(dateString+" "+startDate.toLocaleTimeString());
                            $("#detail-monthly").html(thatIncome + CURRENCY);
                        }
                        
                        if( TICK && ( sofar % TIMERLAP === 0 ) ){
                            handleTicking(sofar);
                        }
                        
                        $("#"+identifier+" span.ui-li-count").html(sofar + CURRENCY);
                    } else {
                        debug("Creating UI element");
                        ident   =   i + "-counter";
                        newli   =   $("<li id=\""+ident+"\" class=\"counterli\">");
                        $(newli).html("<a href=\"#detail\" data-transition=\"slide\">"+thatName+"</a><span class=\"ui-li-count\">?</span>");
                        $(newli).click(function(){
                           detailId = $(this).attr("id");
                        });
                        $(newli).insertBefore("#lastListElement");
                        $("#counterList").listview("refresh");
                    }
                }
            }
            
            /**
             * Teste si le navigateur supporte les notifications,
             * demande la permission de les utiliser si oui
             */
            function testBrowserNotification(){
                debug("Testing browser's notifications");
                if (window.webkitNotifications) {
                    NOTIF_CAPABLE   =   true;
                    $("#notificationStatus").html("Great! Your browser supports \n\
notifications. You can use then to be informed when you earn a specific amount of money (see below)");
                }else{
                    $("#notificationStatus").html("Too bad, no notifications with\n\
 that browser. Try with another one? (Firefox Mobile is good with that");
                }
            }
            
            
            //Misc event listeners
            $(document).ready(function(){
            
                //On start, read value from wherever it's stocked and display
                readParamsValue();
                testBrowserNotification();
                readCounterValue();                    
                refreshCounterList();
                
                //Confirm new counter
                $("#validateNewCounter").on("click",function(){
                    addCounter();
                });
                
                //Toggle add button display on main screen
                $("#show-button").on("click",function(){
                    DISPLAY_BUTTON = true;
                    saveParamValue();
                });
                $("#hide-button").on("click",function(){
                    DISPLAY_BUTTON = false; 
                    saveParamValue();
                });
                
                //Change currency
                $("#currency-selector").on("change",function(){
                    CURRENCY = $(this).val();
                    saveParamValue();
                });
                
                //Detail buttons handling
                $("#detail-reset").on("click",function(){
                    resetCounter(detailId);
                });
                $("#detail-delete").on("click",function(){
                    removeCounter(detailId);
                    $("li.counterli").remove();
                    refreshCounterList();
                    window.location.hash = "";
                    return true;
                });
                
                
                //Param for ticking
                $("#tick-button").click(function(){
                    TICK = true;
                    saveParamValue();
                    if (NOTIF_CAPABLE) {
                        webkitNotifications.requestPermission();
                    }
                    
                });
                $("#notick-button").click(function(){
                    $("#tickerParam").hide();
                    TICK = false;
                    saveParamValue();
                });
                
                //Param for notifying
                $("#notif-button").click(function(){
                    if (NOTIF_CAPABLE) {
                        webkitNotifications.requestPermission();
                    }
                    NOTIF = true;
                    saveParamValue();
                });
                $("#nonotif-button").click(function(){
                    NOTIF = false;
                    saveParamValue();
                });
                
                $("#timerLap").on( 'change', function(){
                    debug("saving timelap");
                   TIMERLAP = $(this).val();
                   saveParamValue();
                });
                
                $("#askPermission").on("click",function(){
                    debug("Pls?");
                    window.webkitNotifications.requestPermission();
                });
                
                //Every second, refresh the infos
                setInterval(function(){
                    refreshCounterList();
                }, 500);

            });