#myTwoCents:
![Detailed counter image](http://blogs.wefrag.com/Hubebert/files/2012/08/kawa.png)
myTwoCents is a mobile web application using the jQueryMobile framework, and using sessionStorage or Cookies to save data between visits. 
The goal of the application is to track the money you earn each second using counters.

#Usage:
Go to the config panel, tap "Add counter". Name it, input your monthly income. Watch your wallet grow.

#Options:
You can ask myTwoCent to notify you every # currency. Each time you earn # currency, you will be notified with a sound and a display.

#Links:
Presentation (in french) on my [blog](http://blogs.wefrag.com/Hubebert/2012/08/20/my-two-cents-site-mobile-inutile/)
Demo of the project is [hosted here](http://epic-code.eu/area51//myTwoCents/)
